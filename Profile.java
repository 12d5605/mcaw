package MCAWbackup;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author Sakiko
 *
 */
public class Profile {
	private String name;
	private Map<Integer, List<Node>> nHash;
	private Map<Integer, Position> inposHash;
	private Map<Node, String> nodeNumInPKCF;/* conserve position-node number in PKCF (e.g. 1-1)*/
	private HashMap<String, Integer> orderHash;//グリカンの名前と順番を記録
	private HashMap<String, Glycan> glycans;
	private HashMap<Position,Integer> markHash;
	protected Position rootPosition;

	public Profile(String name) {
		this.name = name;
		String[] names = name.split("-");
		orderHash = new HashMap();
		int order = 1;
		for(int i = 0; i < names.length; i++){
			this.setOrder(names[i], order++);
		}
		nHash = new TreeMap<Integer, List<Node>>();
		inposHash = new TreeMap<Integer, Position>();
		nodeNumInPKCF = new HashMap<Node, String>();
		markHash = new HashMap<Position, Integer>();
		glycans = new HashMap<String, Glycan>();
	}
	public Profile(Glycan g){
		this(g.getName());

//		for (int i = 1; i <= g.nodelist.size(); i++){
//			System.out.println(g.nodelist);
//			this.addNode(i, g.rootNode);
//		}

	}

	public Glycan[] getGlycans() {
		return (Glycan[])glycans.values().toArray(new Glycan[0]);
	}


	/**
	 * refer the position list which applicable position, add a node at the list.
	 * 2 nodes from KCF1 and KCF2 at same position.
	 * @param pint ...nunber of position of sugar
	 * @param node ... additional node
	 */
	public Position addNode(int pint, Node node) {
		List<Node> nodes;
		Position pos;
		//pint ++;
		if (!inposHash.containsKey(pint)) {
			pos = new Position(this);
			this.setMark(pos, 0);
//			markHash.put(pos, 0); //ProfileにaddNodeした時、positionにmarkをつける
		}else{
			pos = inposHash.get(pint);
		}
		pos.setNumber(pint);
		((Position)pos).addNode(node);
		inposHash.put(pint, pos);
		if (nHash.containsKey(pint)) {
			nodes = nHash.get(pint);
		} else {
			nodes = new ArrayList<Node>();
		}
		if (node != null) {
			nodes.add(node);
		}
		Glycan pglycan = node.getParentGlycan();
		if (!glycans.containsValue(pglycan)){
			glycans.put(pglycan.getName(),(Glycan)pglycan);
		}
		//System.out.println(node.getParentGlycan().getName() +": "+ node.toString() + " will add nodes at " + pint);

		if(pos.getParent()==null && node.getNodeType()==0){
			System.err.println("Parent of this pos is null!!! "+pos.getNumber());
			Node pnode = (Node)node.getParent();
			if (pnode == null) System.err.println("Parent of added node is null!!! "+node.getNumber());
			else {
				Position ppos = pnode.getParentPosition();
				pos.setParent(ppos);
			}
		}
		node.setParentPosition(pos);
		nHash.put(pint, nodes);
		return pos;
	}
	public void printNodes(int pint) {
		List<Node> nodes = nHash.get(pint);
		System.out.println(nodes.toString());
	}

	public void removeNode(int pint, Node node) {
		List<Node> nodes = null;
		Position pos = null;
		if (inposHash.containsKey(pint)) {
			pos = inposHash.get(pint);
			((Position)pos).removeNode(node);
			inposHash.put(pint, pos);
		} else {
			//System.err.println("No such pint found "+pint);
			//System.exit(-1);
		}

		if (nHash.containsKey(pint)) {
			nodes = nHash.get(pint);
			if (node != null) {
				nodes.remove(node);
			}
			nHash.put(pint, nodes);
		} else {
			//System.err.println("No such pint found "+pint);
			//System.exit(-1);
		}


	}

	/**
	 * in case of profile, multiple name is given by following alignment order.
	 * @param gname
	 * @param order
	 */
	public void setOrder(String gname, int order){
		orderHash.put(gname, order);
	}

	/**
	 * obtain order from node. (e.g.key=node, value=1-1)
	 * @param node
	 * @return
	 * @throws Exception
	 */
	public String getNodeNumInPKCF(Node node){
		if (nodeNumInPKCF.containsKey(node)){
			return( nodeNumInPKCF.get(node));
		} else {
			// error - node is not in nodeNumInPKCF!!!
			System.err.print("nodeNum contains"+nodeNumInPKCF.toString());
			System.err.print(" node is "+node.toString());
			System.err.println(" and "+node.getSugar()+", "+node.getNodeType()+", "+node.getParentProfile().getName());
			//throw new Exception("Error - node not found in PKCF");
			return "";
		}
	}

	/**
	 * obtain number of position
	 * @return
	 */
	public int getNumNodes() {
		return nHash.keySet().size();
	}

	/**
	 *obtain list of node from position number(int)
	 * @param position number...key
	 * @return List<node>...value
	 */
	public List<Node> getNodesAt(int position) {
		return nHash.get(position);
	}

	public void setRootPosition(Position pos) {
		this.rootPosition = pos;
		pos.setLayer(0);
	}

	public boolean isRootPosition(Position pos) {
		if(pos == rootPosition){
			return true;
		}else
			return false;
	}
	public Position getRootPosition() {
		return rootPosition;
	}

	public List<Node> getAllNodes(){
		List<Node> nHashValues = new ArrayList();
		for(Iterator i = nHash.keySet().iterator(); i.hasNext();){
			List<Node> nHashValue	= nHash.get(i.next());
			for(int j = 0; j < nHashValue.size(); j++){
				nHashValues.add(nHashValue.get(j));
			}
		}
		return (nHashValues);
	}

	public Position getPositionAt(int i){
		Position position = (Position)inposHash.get(i);
		return (position);
	}

	public void setName(String name) {
		String oldname = this.name;
		Integer value = this.orderHash.get(oldname);
		this.orderHash.remove(oldname);
		this.name = name;
		this.orderHash.put(this.name, value);
	}

	public String getName() {
		return name;
	}

	/**
	 * Make a profileKCF from ArrayList backtrack.at addNode, inposHash and nHash are made.
	 * @return
	 */
	public String toSTRING() {
		String profile = "";
		String edgepkcf ="";//20110516
		// System.out.println("phashValues" + pHash.values().toString());
		int keySizeOfnHash = nHash.keySet().size(); /*same as inposHash?*/
		int haba = 0;
		//Position rootPosition = (Position) inposHash.get(1);
		String[] names = name.split("-");
		/******** ProfileKCF Design *********/
		profile += "ENTRY" + Spacer(7) + name + Spacer(22) + "GlycanProfile\n";
		profile += "NODE" + Spacer(8) + keySizeOfnHash + "\n";
		edgepkcf += "EDGE" + Spacer(8) + (keySizeOfnHash - 1) + "\n";//20110516
		String anomerPattern = "[ab]";//20110526
		int edgeNum = 0;
		for (Iterator i = nHash.keySet().iterator(); i.hasNext();) {
			int pint = (Integer) i.next();
			Position p = inposHash.get(pint);

			List nodes = p.getNodes(); //nHash.get(pint);//nodes at the pint. nodesはピントにあるノード達
			List sortorderList = orderHashSortList(nodes);//orderHashのソートリストをつくる
			//sortorderList = nodes;

			for (int j =0; j<sortorderList.size(); j++){//ソートリストからノードとそのノードのエッジをとりだす。
				int order = (Integer) j+1;
				Node n = (Node) sortorderList.get(j);

				//TODO: pint+1 only for profiles
				String value = (pint+1) + "-" + order;//★pint+1の"＋１"いるかも110106
				//System.out.println("key: " + n + "value: " + value);
				nodeNumInPKCF.put(n, value);//key=そのpint(ポジション)にある糖鎖のノード(プロファイルの場合のy番目のノード),value="そのポジション"－"糖鎖のオーダー(番目)"
			}
		}
		for (Iterator i = nHash.keySet().iterator(); i.hasNext();) {
			int pint = (Integer) i.next();
			Position p = inposHash.get(pint);

			//TODO make comparator to sort nodes automatically
			List nodes = p.getNodes(); //nHash.get(pint);//nodes at the pint. nodesはピントにあるノード達

			if(p.getParent() != null){
				edgeNum ++;
			}

			profile += Spacer(12) + (pint+1) + Spacer(5);

			List sortorderList = orderHashSortList(nodes);//orderHashのソートリストをつくる
			//sortorderList = nodes;

			for (int j =0; j<sortorderList.size(); j++){//ソートリストからノードとそのノードのエッジをとりだす。
				int order = (Integer) j+1;
				Node n = (Node) sortorderList.get(j);

				//String value = (pint) + "-" + order;//★pint+1の"＋１"いらないかも110106
				//System.out.println("key: " + n + "value: " + value);
				//nodeNumInPKCF.put(n, value);//key=そのpint(ポジション)にある糖鎖のノード(プロファイルの場合のy番目のノード),value="そのポジション"－"糖鎖のオーダー(番目)"
				int ntype = n.getNodeType();
				if(ntype == 1 || ntype ==2){/*ntype 0=residue , 1=gap , 2=missing*/
					String state = "";
					if(ntype == 1){// 1=gap
						state = "-";
					}else if (ntype == 2){// 2=missing
						state = "0";
					}
					profile +=  order+ "=" + state  + Spacer(3);
				}else{
					profile +=  order+ "="	+ n.getSugar() + Spacer(3);
				}
				//if (y == nodesSize - 1) { /* Last line*/
				if (j == sortorderList.size()-1){
					profile += Spacer(4)	+ p.getXCoordinate() + Spacer(4) + p.getYCoordinate() + "\n";
				}
				/*  EDGE   */
				if(p.getParent() != null){//20110516
					edgepkcf += Spacer(12) + edgeNum + Spacer(5);//EDGEナンバー(pint番目).pint番目のエッジ情報をいれていく
					try {
						edgepkcf += getNodeNumInPKCF(n);//結合の子ノード番号(n) － プロファイル順
						if((String) n.getAnomer() != null && ((String) n.getAnomer()).matches(anomerPattern) || n.getMyNum() != -1){
							edgepkcf += ":";//子ノードにアノマー情報があれば：を入れる
						}
						if(((String) n.getAnomer()) != null){
							edgepkcf += n.getAnomer();//子ノードにアノマー情報があればアノマーを入れる
						}
						if(n.getMyNum() != -1){
							edgepkcf += n.getMyNum();//アノマーの結合位置があれば入れる
						}
						edgepkcf += Spacer(5);
						//if(getNodeNumInPKCF((Node) n.getParent()) != null){//結合の親ノード(nと結合する親)があれば親ノード番号 － プロファイル順をいれる
//						System.out.println("●●p =  "+p);
//						System.out.println("getParent=  "+p.getParent());
//						System.out.println("myNodes=  "+p.getParent().myNodes);
//						System.out.println("getGlycan=  "+n.getParentGlycan());
//						System.out.println("getName=  "+n.getParentGlycan().getName());
//						System.out.println("Node=  "+p.getParent().myNodes.get(n.getParentGlycan().getName()));
						edgepkcf += getNodeNumInPKCF((Node)(p.getParent().myNodes.get(n.getParentGlycan().getName())));
						//}
						if(n.getParentNum() != -1){
							edgepkcf += ":" + n.getParentNum();
						}
						edgepkcf += "\n";
					} catch (Exception e) {
						//nodeNInPCKF ="";
						// TODO 自動生成された catch ブロック
						e.printStackTrace();
					}
				}
			}
		}
		profile += edgepkcf;//20110526

		profile += "///\n";
		return profile;
	}
	/**
	 * (reflexive subroutine) calculate haba from number of positions. pos as parent, poschildren is child of pos.
	 * @param pos
	 */
	public void habaSetting(Position pos){
		List<Position> posChildren = pos.getChildren();
		int childHaba = 0;
		int posCSize = posChildren.size();
		//System.out.println("in Haba setting" + posCSize);
		//printLayer(pos);
		if (posCSize > 0){/*have children*/
			for(int j = 0; j < posCSize; j++){
				Position poschildren_elem = (Position) posChildren.get(j);
				if (poschildren_elem.getLayer() <= pos.getLayer()) {
					//System.err.println("Problem with layers");
//					System.out.println("poschildren:"+poschildren_elem.getLayer());
//					System.out.println("pos        :"+pos.getLayer());
				}
				habaSetting(poschildren_elem);
				childHaba += poschildren_elem.getHaba();/*calculate sum*/
				//printLayer(pos,poschildren_elem);
				//printLayer(poschildren_elem);
			}
			int myHaba = childHaba + 2 * (posCSize - 1);/* parent(haba) = 竏舛(haba) + (C-1)*2 */
			//System.out.println("Haba of "+pos.getNodes() + " is " + myHaba);
			pos.setHaba(myHaba);
		}else{/*posCSize ==0*/
			//System.out.println(pos.getNodes().toString() + "will set as haba 0");
			pos.setHaba(0);
		}
	}




	public void printLayer(Position pos,Position p){//Layerを見る関数
		List<Node> posiListNodes = new ArrayList();
		posiListNodes = (List) p.getNodes();
		for(int j =0; j<posiListNodes.size(); j++){
				//System.out.println("Position parent "+pos.number+": "+" Layer:"+pos.getLayer()+" Position child "+p.number+": "+" Layer:"+p.getLayer());
				//System.out.println("Childposition:"+p.getChildren().get(j).getLayer());
		}
	}

	/**
	 * (reflexive subroutine) setting x and y coordinate of children.
	 * @param p
	 */
	public void xyCoordinateSetting(Position p){
		/*x = x(parent)-8*/
		/*y = haba/(number of position)*/
		int konosa = 0;
		int parentY = p.getYCoordinate();
		List<Position> children = p.getChildren();
		//System.out.println( "xyCoordinate children size" + children.size());
		int numChildren = children.size();
		int habaOfParent = p.getHaba();
		int x = p.getXCoordinate()-8;
		int childY = 0;
		if(numChildren > 1){
			konosa = habaOfParent/(numChildren-1);
		}
		for (int j = 0; j < numChildren; j++) {
			Position childpos = (Position)children.get(j);
			childpos.setXCoordinate(x);
			if(numChildren == 1){
				childY = parentY;
			}else{
				if (j == 0) {
					childY = parentY-(habaOfParent/2);
				}else{
					childY = childY + konosa;
				}
			}
			childpos.setYCoordinate(childY);
			xyCoordinateSetting(childpos);
		}
	}

	/**
	 * convert String int int
	 * @param str
	 * @return
	 */
	public boolean isNumber(String str){
		try {
			int input = Integer.parseInt(str);
		} catch (NumberFormatException e){
			return false;
		}
		return true;
	}

	public String Spacer(int x) {
		int timesOfSpacing = x;
		String str = "";
		for (int i = 0; i < timesOfSpacing; i++) {
			str += " ";
		}
		return str;
	}

	/**
	 * Compute number of positions
	 * @return number of positions in this profile
	 */
	public int getNumOfPosition(){
		int numOfPosition = (inposHash.keySet()).size();
		return numOfPosition;
	}

	public void setMark(Position pos, int mark){ //マークをつける関数,複数のポジションがあった場合１つずつノードのポジションにマークを付ける

//		if (0<pos.myNodes.keySet().size()){
//			for (Iterator it=pos.myNodes.keySet().iterator(); it.hasNext(); ){
//				it.next();
//				it.
//			}
//		}そのglycanを呼び出して引数で受け取ったポジションposをセットマークしたらどうか？
//		pos.myNodes.keySet();
		System.out.println(this.getName()+" setting mark of position "+pos.getNumber()+" to "+mark);
		markHash.put(pos, mark);
	}

	public void printMarks() {
		System.out.println("I am Profile "+this.name);
		for (Iterator it = markHash.keySet().iterator(); it.hasNext();){
			Position p = (Position)it.next();
			System.out.println(p.getNumber() + " has mark "+markHash.get(p));
		}
	}

	public void clearMark(){
		markHash.clear();
	}

	public ArrayList<Position> PosListWithMark(int m){
		ArrayList<Position> markposi = new ArrayList<Position>();
		Iterator<Position> HashMark = markHash.keySet().iterator();
		while (HashMark.hasNext()){
			Position pmark = HashMark.next();
			if (markHash.get(pmark)== m){
				markposi.add(pmark);
			}
		}
		Collections.sort(markposi, new MarkListComparator());//110415markposiのリストをポジション順に並べ替える
		return markposi;
	}
	public List orderHashSortList( List nodes){ //orderHashのソートリストをつくる.nodesを１つづつ取り出してGlycanの名前と順番をそろえる
		//nodesは
		ArrayList sortlistnodes = new ArrayList();
		String name="";//Glycanの名前

		if (sortlistnodes.size()<orderHash.size()){
			for (Iterator it=orderHash.keySet().iterator(); it.hasNext(); ){
				it.next();
				sortlistnodes.add(null);
			}
		}
		//System.out.println("N1="+nodes.toString());
		//System.out.println("S1="+sortlistnodes.toString());


		for(int j=0; j <nodes.size(); j++ ){
			Node node = (Node) nodes.get(j);
			name = ((Glycan) node.getParentGlycan()).getName();
			int k = orderHash.get(name);
			Node setnode = null;
			if ((setnode  = (Node)sortlistnodes.get(k-1)) == null){
				sortlistnodes.set(k-1, node);
			} else { // already set!!!
				System.err.println("Trying to set "+(k-1)+" to "+node.getSugar()+"("+node.getNodeType()+") when it is already set to "+setnode.getSugar());
			}

		}

		for (int i=sortlistnodes.size()-1; i>=0; i--) {
			Node n = (Node)sortlistnodes.get(i);
			if (n==null) {
				sortlistnodes.remove(i);
			}
		}

		return sortlistnodes;
	}
}