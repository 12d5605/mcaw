package MCAWbackup;

import java.util.Comparator;

public class MarkListComparator implements Comparator {
	public int compare(Object arg0, Object arg1) { //110415ポジション順に並べ替える関数
		 if (!(arg0 instanceof Position) || !(arg1 instanceof Position)) {
	            throw new IllegalArgumentException("arg0 & arg1 must implements interface of Position.");
	        }
		 int p1 = ((Position)arg0).getNumber();
		 int p2 = ((Position)arg1).getNumber();
		 if (p1 == p2) {
			 return 0;
		 } else if (p1 > p2) {
			 return 1;
		 } else {
			 return -1;
		 }
		 //0：等しい。1：より大きい。-1：より小さい
    }
}
