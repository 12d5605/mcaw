package MCAWbackup;



import java.util.ArrayList;

import java.util.HashMap;

import java.util.HashSet;

import java.util.Iterator;

import java.util.List;





public class MCAW {

	private  int gapPenaltyscore =-10;
	private  int reCarbonscore = 20;
	private  int nreCarbonscore = 20;
	private  int anomerscore = 20;
	private  int sugarscore = 60;

	ArrayList entry = new ArrayList();
	HashMap scores = new HashMap();
	HashMap mappings = new HashMap();
	ArrayList backtrack = new ArrayList();

	double allMaxScore = 0;

	String allMaxPair = "";




	/**

	 * @param args

	 */
	public MCAW(){
		super();
	}


	public MCAW(int gap, int sugar, int anomer, int nre, int re ) {//constructor

		super();
		gapPenaltyscore = gap;
		sugarscore = sugar;
		anomerscore = anomer;
		nreCarbonscore = nre;
		reCarbonscore = re;


	}



/**

 * main program of MCAW.java. alignments are execute here.

 * @param g1
 * @param g2
 * @param w
 * @return

 */

	public Profile doAlignment(Glycan g1, Glycan g2, HashMap w) {

		String arrangedPKCF = "";
		double maxScore = 0;

		ArrayList mapping = new ArrayList();
		ArrayList returned = new ArrayList();

		Node rootNode1 = g1.getRootNode();
		Node rootNode2 = g2.getRootNode();

		this.scores.clear();
		this.mappings.clear();
		this.backtrack.clear();
		this.allMaxScore = 0;
		this.allMaxPair = "";

		returned = this.getLinkScore(rootNode1, rootNode2, w, maxScore, mapping);
		//System.out.println("allMaxScore: " + this.allMaxScore + " and allMaxPair: " + this.allMaxPair);

		mapping = (ArrayList) mappings.get(this.allMaxPair);
		//System.out.println("mappings are" + mapping.toString()); //2-2

		String name1 = g1.getName();
		String name2 = g2.getName();

		Profile PKCF = new Profile(name1 + "-" + name2);
		String[] keys = this.allMaxPair.split("-");

		int keys0 = Integer.parseInt(keys[0]);
		int keys1 = Integer.parseInt(keys[1]);
		int count = 0;//pint

		List<Node> nodes1 = g1.getNodesAt(keys0);
		List<Node> nodes2 = g2.getNodesAt(keys1);

		Node node1 = nodes1.get(0);
		Node node2 = nodes2.get(0);

		node1.setNodeType(0); //0 = residue
		node2.setNodeType(0);

		Position pos = PKCF.addNode(1, node1);
		pos = PKCF.addNode(1, node2);
		PKCF.setRootPosition(pos);/*rootPosition might change depends on the existence of additionalNode.*/

		node1.setParentPosition(pos);
		node2.setParentPosition(pos);

		Position posclone = pos;/*copying pos->posclone*/
		ArrayList parents = new ArrayList();

		parents.add(node1);
		parents.add(node2);
		int x = 0;


		/*in case of addition of nodes at root*/

		if(node1.getLayer() != 0){

			while(node1.getLayer() != 0){
				count = count + 1;
				Node pnode1 = (Node) node1.getParent();
				ArrayList node_and_pos = additionalNodeAtRoot(PKCF, pnode1, g2, count);
				node1 = (Node) node_and_pos.get(0);
				pos = (Position) node_and_pos.get(1);
				PKCF.setRootPosition(pos);
			}

		}else if(node2.getLayer() != 0){

			while(node2.getLayer() != 0){
				count = count + 1;
				Node pnode2 = (Node) node2.getParent();
				ArrayList node_and_pos = additionalNodeAtRoot(PKCF, pnode2, g1, count);
				node2 = (Node) node_and_pos.get(0);
				pos = (Position) node_and_pos.get(1);
				PKCF.setRootPosition(pos);
			}
		}

		printMappings(mapping, parents, posclone);


		/*check all nodes whether included as a position of PKCF.
		if nodes reminds at leaf, they will include as positions.*/

		List<Node> nodeListOfKCF1 = new ArrayList();
		List<Node> nodeListOfKCF2 = new ArrayList();
		List<Node> nodeListOfPKCF = new ArrayList();

		nodeListOfKCF1 = g1.getAllNodes();
		nodeListOfKCF2 = g2.getAllNodes();
		nodeListOfPKCF = PKCF.getAllNodes();


		//System.out.println("rootPosition have "+PKCF.rootPosition.getNodes());
		//checkPositionElement(PKCF.rootPosition);
		//System.out.println("currentPKCF is "+nodeListOfPKCF.toString());


		for(Iterator i = nodeListOfKCF1.iterator(); i.hasNext();){
			Node inext = (Node) i.next();

			if(nodeListOfPKCF.contains(inext)){
				i.remove();
			}
		}

		for(Iterator i = nodeListOfKCF2.iterator(); i.hasNext();){
			Node inext = (Node) i.next();

			if(nodeListOfPKCF.contains(inext)){
				i.remove();
			}
		}

		//System.out.println("remined contents in glycan1: " + nodeListOfKCF1.toString());
		//System.out.println("remined contents in glycan2: " + nodeListOfKCF2.toString());

		int offset = PKCF.getNumOfPosition();


		/*in case of addition nodes at leaf part.*/

		if(nodeListOfKCF1.size() != 0){
			additionalNodeAtLeaf(PKCF, nodeListOfKCF1, g2, offset);
		}

		if(nodeListOfKCF2.size() != 0){
			additionalNodeAtLeaf(PKCF, nodeListOfKCF2, g1, offset);
		}

		PKCF.habaSetting(PKCF.rootPosition);/*setting of Haba*/
		PKCF.xyCoordinateSetting(PKCF.rootPosition); 	 /*setting of xy coordinate*/

		return PKCF;

	}

	/**
	 * main program of MCAW.java. alignments are execute here.
	 * @param Profile p1
	 * @param Profile p2
	 * @param w
	 * @return
	 */

		public Profile doAlignment(Profile p1, Profile p2, HashMap w) {

			//String arrangedPKCF = "";
			double maxScore = 0;

//			g1.clearMark();
//			g2.clearMark();

			ArrayList mapping = new ArrayList();
			ArrayList returned = new ArrayList();

			Position rootNode1 = p1.getRootPosition();
			Position rootNode2 = p2.getRootPosition();
			List rootnodenodes = rootNode1.getNodes();
			//System.out.println(rootnodenodes.size());

			this.scores.clear();
			this.mappings.clear();
			this.backtrack.clear();
			this.allMaxScore = 0;
			this.allMaxPair = "";
			returned = this.getLinkScore(rootNode1, rootNode2, w, maxScore, mapping);

			//System.out.println("allMaxScore: " + this.allMaxScore + " and allMaxPair: " + this.allMaxPair);
			mapping = (ArrayList) mappings.get(this.allMaxPair);
			//System.out.println("mappings are" + mapping.toString()); //2-2

			String name1 = p1.getName();
			String name2 = p2.getName();
			String[] keys = this.allMaxPair.split("-");

			int keys0 = Integer.parseInt(keys[0]);
			int keys1 = Integer.parseInt(keys[1]);

			int count = 0;//pint;

			List<Node> nodes1 = p1.getNodesAt(keys0);
			List<Node> nodes2 = p2.getNodesAt(keys1);

			Position posinodes1 = p1.getPositionAt(keys0);//Profile g1のkeys0のposition
			Position posinodes2 = p2.getPositionAt(keys1);//Profile g2のkeys1のposition allmaxpareのposition

			ArrayList maxPositions = new ArrayList();
			maxPositions.add(posinodes1);
			maxPositions.add(posinodes2);

			p1.setMark(posinodes1, 1);//PKCFに入れたg1のpositionにマークをつける
			//for (int i = 0; i<nodes1.size(); i++ ){
				//Node node1 = nodes1.get(i);//0番目のノード
				////node1.setNodeType(0); //0 = residue
			//}
			p2.setMark(posinodes2, 1);//PKCFに入れたg2のpositionにマークをつける
			//for (int i = 0; i<nodes2.size(); i++ ){
				//Node node2 = nodes2.get(i);//0番目のノード
				////node2.setNodeType(0); //0 = residue
			//}
//			Node node1 = nodes1.get(0);//0番目のノード
//			Node node2 = nodes2.get(0);//0番目のノード
//			node1.setNodeType(0); // 0= residue, 1= gap, 2= missing
//
//			node2.setNodeType(0);


//getlinkscore no ato no pos 100630
			Profile PKCF = new Profile(name1 + "-" + name2); //makePKCF
			Position pkcfrootpos = null;
			//ArrayList parents = new ArrayList();

			this.checkPositionParents(PKCF);

			for (int i = 0; i < nodes1.size(); i++){  //(ピント0にあるProfileg1に入ってるノードを全て繰り返す(マックススコアポジションのノードをPKCFに入れる)
				pkcfrootpos = PKCF.addNode(0, nodes1.get(i));//ピント0に対して※positionとnodeのハッシュをaddNodeでつくり、positionをposに入れる。
				nodes1.get(i).setParentPosition(pkcfrootpos);//Profileg1に入ってるノードのsetParentPosition
				//parents.add(nodes1.get(i));//parentsアレイリストにProfileg1のノードを入れる
			}

			for (int i = 0; i < nodes2.size(); i++){  //(ピント0にあるProfileg2に入ってるノードを全て繰り返す(マックススコアポジションのノードをPKCFに入れる)
				pkcfrootpos = PKCF.addNode(0, nodes2.get(i));//ピント0に対して※positionとnodeのハッシュをaddNodeでつくり、positionをposに入れる。(PKCFの名前順じゃなくなってる。node1の後ろに入らず間に入ったりしてる2012/11/23)
				nodes2.get(i).setParentPosition(pkcfrootpos);//Profileg2に入ってるノードのsetParentPosition
				//parents.add(nodes2.get(i));//parentsアレイリストにProfileg2のノードを入れる
			}

			PKCF.setRootPosition(pkcfrootpos);/*rootPosition might change depends on the existence of additionalNode.*///(マックススコアポジションをプロファイルのルートポジションと設定してしまっていいのだろうか？)

			int x = 0;

			this.checkPositionParents(PKCF);


			/*in case of addition of nodes at root*/

				//while(posinodes1.getLayer() != 0){//MaxPareのkeys0(g1のposition)のlayerが0じゃないの時
			    while (p1.getRootPosition().getNumber() != posinodes1.getNumber()){

					Position pnode1 = posinodes1.getParent();//
					count = count + 1;
					//g1.SetMark(pnode1, 1);//mark(1)をつける

					ArrayList node_and_pos = additionalNodeAtRoot(PKCF, pnode1, p2, count);//missingノードを作るかg2のノードをPKCFのrootに追加する
					posinodes1 = (Position) node_and_pos.get(0);
					posinodes2 = (Position) node_and_pos.get(1);//g2の新しいルートを取得し次のwhile文でそのまま実行できるようにした。20110527
				}


				//while(posinodes2.getLayer() != 0){
			    while(p2.getRootPosition().getNumber()!=posinodes2.getNumber()) {

					Position pnode2 = posinodes2.getParent();
					count = count + 1;
					//g2.SetMark(pnode2, 1);//mark(1)をつける
					if (pnode2 == null) {
						System.err.println("MCAW.additionalNodeAtRoot: error!! maxposparent is null!");
					}
					ArrayList node_and_pos = additionalNodeAtRoot(PKCF, pnode2, p1, count);//missingノードを作りPKCFのrootに追加する
					posinodes2 = (Position) node_and_pos.get(0);
					//pkcfrootpos = (Position) node_and_pos.get(1);
					//PKCF.setRootPosition(pkcfrootpos);

				}

			this.checkPositionParents(PKCF);

			printMappings(mapping, maxPositions, pkcfrootpos); //clone);//alignmentされた部分をPKCFに入れる

			this.checkPositionParents(PKCF);


			/*check all nodes whether included as a position of PKCF.

			if nodes reminds at leaf, they will include as positions.*/

			ArrayList nonmarkposilist1 = new ArrayList();
			ArrayList nonmarkposilist2 = new ArrayList();
			nonmarkposilist1 = p1.PosListWithMark(0);//PKCFに入れていないノードのポジションをArrayListととして取る
			System.out.println("[1]nonmarkpos :"+nonmarkposilist1.size());

			p2.printMarks();
			nonmarkposilist2 = p2.PosListWithMark(0);//PKCFに入れていないノードのポジションをArrayListととして取る
			System.out.println("[2]nonmarkpos :"+nonmarkposilist2.size());
//			if(nonmarkposilist1.size() != 1){
//				Node[] narray =new Node[nonmarkposilist1.size()];
//				for(int i=0;i<nonmarkposilist1.size();i++){
//				  narray[i] = (Node)nonmarkposilist1.get(i);
//				 }
//
//			}

			/*in case of addition nodes at leaf part.*/
			if(nonmarkposilist1.size() != 0){//マークが付いていない(PKCFに入っていない)ノードのポジションのリストをadditionalNodeAtLeafに渡す
				int offset = PKCF.getNumOfPosition();
				System.out.println("[-1-]nonmarkpos :"+nonmarkposilist1.size());
				additionalNodeAtLeaf(PKCF, nonmarkposilist1, p2, offset);
				System.out.println("[--1--]nonmarkpos :"+nonmarkposilist1.size());
			}
			if(nonmarkposilist2.size() != 0){//マークが付いていない(PKCFに入っていない)ノードのポジションのリストをadditionalNodeAtLeafに渡す
				int offset = PKCF.getNumOfPosition();
				System.out.println("[-2-]nonmarkpos :"+nonmarkposilist2.size());
				additionalNodeAtLeaf(PKCF, nonmarkposilist2, p1, offset);
				System.out.println("[--2--]nonmarkpos :"+nonmarkposilist2.size());
			}
			this.checkPositionParents(PKCF);



			PKCF.habaSetting(PKCF.rootPosition);/*setting of Haba*/
			PKCF.xyCoordinateSetting(PKCF.rootPosition); 	 /*setting of xy coordinate*/

			//printLayer(PKCF.rootPosition);

			p1.clearMark();
			p2.clearMark();

			this.checkPositionParents(PKCF);

			return PKCF;

		}

	private void checkPositionParents(Profile prof) {
		if (!prof.getName().contains("-")) {
			return;
		}
		int np = prof.getNumOfPosition();
		for (int i=0; i<np; i++) {
			Position pos = prof.getPositionAt(i);
			if (pos == null) {
				System.out.println("Null position found");
			}
			if (prof.getRootPosition() != pos && pos.getParent() == null) {
				System.out.println("Null parent found");
			}
		}
	}


	/**comfirm the elements of one position.
	 * @param root
	 */

	private void checkPositionElement(Position root){

		ArrayList pos_children = (ArrayList) root.getChildren();

		for (int i = 0; i < pos_children.size(); i++){

			Position pelem = (Position) pos_children.get(i);
			//System.out.println("my position have " + pelem.getNodes());

			if(pelem.getChildren().size() > 0){
				checkPositionElement(pelem);
			}
		}
	}



	/**
	 * add a node as root position of PKCF
	 * @param PKCF
	 * @param node
	 * @param partnerGlycan
	 * @param i pint of position in pkcf, initially 1 because only root has been added
	 * @return
	 */

	private ArrayList additionalNodeAtRoot(Profile PKCF, Node node, Glycan partnerGlycan, int i) {

		//System.out.println("●+++ find missingNode at Root");
		Position pos = null;
		ArrayList node_and_pos = new ArrayList();
		int num_of_nodes = partnerGlycan.getNumNodes();
		Position primaryRoot = PKCF.rootPosition;

		//System.out.println("primaryRootPosition have " + primaryRoot.getNodes());
		//Node rootOfMissing = partnerGlycan.getRootNode();//primary root node of missing

		Node missingNode = new Node("", partnerGlycan);
		missingNode.setId(num_of_nodes+1);
		missingNode.setBond("", -1, -1);
		missingNode.setNodeType(2);
		partnerGlycan.addNode(num_of_nodes+1, missingNode);
		partnerGlycan.nodelist.add(missingNode);
		pos = PKCF.addNode(i, node);

		ArrayList<Node> mychildren = (ArrayList) node.getChildren();
		ArrayList<Node> rNodes = (ArrayList) primaryRoot.getNodes();
		ArrayList clone = (ArrayList<Node>) rNodes.clone();

		//System.out.println("rNodes are "+ clone.toString());

		for(int j = 0; j < mychildren.size(); j++){
			for(int k = 0; k < clone.size(); k++){
				if(clone.get(k) == mychildren.get(j)){
					//System.out.println("remove " + clone.get(k) + " from rNodes" );
					clone.remove(k);
				}
				Node remind = (Node) clone.get(0);
				missingNode.addChildNode(remind);
			}
		}

		pos = PKCF.addNode(i, missingNode);
		pos.addChildNode(primaryRoot);

		//System.out.println("check position element");
		//checkPositionElement(pos);

		node_and_pos.add(node);
		node_and_pos.add(pos);

		//System.out.println("+++ end of missingNode at Root");

		return node_and_pos;
	}


	/**
	 * add a node as root position of PKCF
	 * @param PKCF
	 * @param maxposparent : rootがある方のProfileのPosition
	 * @param partnerProfile : rootがない方のProfile
	 * @param i : rootをつけたする数のカウント
	 * @return
	 */

	private ArrayList additionalNodeAtRoot(Profile PKCF, Position maxposparent, Profile partnerProfile, int i) {

		//System.out.println("+++ find missingNode at Root");

		Position pos = null;
		ArrayList node_and_pos = new ArrayList();
		Position primaryRoot = PKCF.getRootPosition();	//PKCFのrootにあるposition
		Glycan[] partnerGlycans = partnerProfile.getGlycans();
		List<Node> nodes = maxposparent.getNodes();	//Position nodeにある糖鎖の数分のnode

		for( int k =0; k<nodes.size(); k++){ //ProfilePKCFにnode(doAlignmentのmaxPerKeyのPositionのノードの親)を入れる
			pos = PKCF.addNode(i, nodes.get(k));
			//nodes.get(k).setNodeType(0);
			maxposparent.getParentProfile().setMark(maxposparent, 1);//110108set mark
			PKCF.setRootPosition(pos);
		}

		//save new position in partnerglycans
		for( int j = 0; j<partnerGlycans.length; j++){	//partnerGlycans(partnerProfile)の方にmissingnodeを入れる
			int num_of_nodes = partnerGlycans[j].getNumNodes();
			//System.out.println("primaryRootPosition have " + primaryRoot.getNodes());
			//Node rootOfMissing = partnerGlycan.getRootNode();//primary root node of missing

			Node missingNode = partnerGlycans[j].addMissingRootNode("");
/*
			ArrayList<Position> mychildren = (ArrayList) maxposparent.getChildren();
			ArrayList<Node> rNodes = (ArrayList) primaryRoot.getNodes();
			ArrayList clone = (ArrayList<Node>) rNodes.clone();

		//System.out.println("rNodes are "+ clone.toString());

			for(int l = 0; l < mychildren.size(); l++){
				for(int k = 0; k < clone.size(); k++){
					if(clone.get(k) == mychildren.get(l)){
						//System.out.println("remove " + clone.get(k) + " from rNodes" );
						clone.remove(k);
					}
					Node remind = (Node) clone.get(0);
					missingNode.addChildNode(remind);
				}
			}
			*/
			pos = PKCF.addNode(i, missingNode);
			//System.out.println("missing position"+missingNode.getParentPosition());
			//Position missingPos = partnerProfile.addNode(partnerProfile.getNumNodes()+1, missingNode);
			//partnerProfile.SetMark(missingNode.getParentPosition(), 1);//110109
			partnerProfile.setRootPosition(missingNode.getParentPosition());

		}

		pos.addChildNode(primaryRoot);

		//System.out.println("check position element");
		//checkPositionElement(pos);

		node_and_pos.add(maxposparent);
		node_and_pos.add(pos);


		//System.out.println("+++ end of missingNode at Root");

		return node_and_pos;
	}



	/**
	 * add a node as leaf position of PKCF
	 * @param PKCF
	 * @param nodeListOfKCF
	 * @param partnerGlycan
	 * @param offset
	 */

	public void additionalNodeAtLeaf(Profile PKCF, List<Node> nodeListOfKCF, Glycan partnerGlycan, int offset) {

		while(nodeListOfKCF.size()!=0){
			//System.out.println("+++missingNode at Leaf");
			int num_of_nodes = partnerGlycan.getNumNodes();
			Node additionalNode = nodeListOfKCF.get(0);
			Node missingNode = new Node("", partnerGlycan);
			missingNode.setId(num_of_nodes+1);
			missingNode.setBond("", -1, -1);
			missingNode.setNodeType(2);
			partnerGlycan.addNode(num_of_nodes+1, missingNode);
			partnerGlycan.nodelist.add(missingNode);
			Node parentNode = (Node) additionalNode.getParent();

			//System.out.println("parent of addtionalNode " + additionalNode.toString() + " is " + parentNode.toString());

			Position parentPosition = parentNode.getParentPosition();
			ArrayList<Node> nodesAtParentPosition = (ArrayList) parentPosition.getNodes();
			//System.out.println("nodes at this position are " + nodesAtParentPosition);
			nodesAtParentPosition.remove(parentNode);

			for(int i = 0; i < nodesAtParentPosition.size(); i++){
				Node remain = nodesAtParentPosition.get(i);
				remain.addChildNode(missingNode);
				//missingNode.setParent(remain);
			}

			Position pos = PKCF.addNode(offset, additionalNode);
			pos = PKCF.addNode(offset, missingNode);
			additionalNode.setParentPosition(pos);
			missingNode.setParentPosition(pos);
			parentPosition.addChildNode(pos);
			nodeListOfKCF.remove(0);
			offset = offset+1;
		}
	}


	/**
	 * add a node as leaf position of PKCF
	 * @param PKCF
	 * @param nodeListOfKCF
	 * @param partnerGlycan
	 * @param offset
	 */

	public void additionalNodeAtLeaf(Profile PKCF, ArrayList nonmarkposilist, Profile partnerProfile, int offset) {

		while(nonmarkposilist.size()!=0){

			Position pos = null;
			Position additionalPosition = (Position) nonmarkposilist.get(0);//PKCFに入ってない1つ目のポジションをadditionalPositionに入れる
			//System.out.println("+++missingNode at Leaf");
			List<Node> additionalNodes = additionalPosition.getNodes();	//Position additionalPositionにある糖鎖の数分のnode(PKCFに入っていない1つ目のポジションにある全てのノード)
			System.out.println(additionalNodes);
			Position parentPosition = additionalPosition.getParent();//additonalPositionの親のポジションをとる

			if (parentPosition == null) {
				System.err.println("Found null parentPosition");
			}
			ArrayList<Node> nodesAtParentPosition = (ArrayList) parentPosition.getNodes();//親ポジションにあるノードをとる
			Node ppnode = nodesAtParentPosition.get(0);//(1)親ポジションにあるノードの1つ目をとり、
			Position pkcfPPosition = ppnode.getParentPosition();//(2)そのポジションをとるとPKCFにあるそのポジションをとってくることになるので、
			List<Node> pkcfNodes = pkcfPPosition.getNodes();//(3)そのポジションにあるノードを全てとる

			for( int k =0; k<additionalNodes.size(); k++){
				Node addNode = additionalNodes.get(k);
				pos = PKCF.addNode(offset, addNode);//ProfilePKCFにアライメントされてないノードを入れる
				addNode.setParentPosition(pos);
			}

			Glycan[] partnerGlycans = partnerProfile.getGlycans();

			for(int j=0; j<partnerGlycans.length; j++){//missingを入れる方のプロファイルの糖鎖にmissingノードを作成する
				int num_of_nodes = partnerGlycans[j].getNodeListSize(); //partnerGlycans[j].getNumNodes();
				Node missingNode = new Node("", partnerGlycans[j]);
				missingNode.setId(num_of_nodes+1);
				missingNode.setBond("", -1, -1);
				missingNode.setNodeType(2);//Type(2)= missing
				partnerGlycans[j].addNode(num_of_nodes+1, missingNode);
				partnerGlycans[j].nodelist.add(missingNode);

				for(int l = 0; l<pkcfNodes.size(); l++){
					Node remain = pkcfNodes.get(l);

					if(remain.getParentGlycan() == partnerGlycans[j]){
						remain.addChildNode(missingNode);
						missingNode.setParent(remain);
						pos = PKCF.addNode(offset, missingNode);//ProfilePKCFにmissingノードを入れる
						//System.out.println(missingNode.getParentGlycan().getName());
						missingNode.setParentPosition(pos);
					}
//					else{
//						System.out.println("Error add missing node at Leaf");
//					}
				}
			}

			pkcfPPosition.addChildNode(pos);

			nonmarkposilist.remove(0);

			offset = offset+1;

		}

	}


	/**
	 * determine positions by backtracking mappings
	 * @param m Mapped Nodes
	 * @param parents Parent nodes of mapped nodes
	 * @param parentPosition Position in PKCF of Mapped nodes
	 */

	public void printMappings(ArrayList<Position> m, ArrayList<Position> parents, Position parentPosition) {

//		System.out.println("Enter printmappings with "+m.toString());
//		System.out.println("Parents are "+parents.toString());
//		System.out.println("parentPosition is " + parentPosition.getNodes().toString());

		//int mapSize = m.size();
		//Profile PKCF = parentPosition.parentProfile;
		//int offset = 1;

		//for (int i=0; i<mapSize; i=i+2) {
			//Position posi1 = (Position) m.get(i);
			//Position posi2 = (Position) m.get(i+1);
		while (m.size() > 1) {
			Position posi1 = (Position)m.get(0);
			Position posi2 = (Position)m.get(1);

			if(posi1 == null|| posi2 == null){
				break;
			}

			//String previouskey = (node1 == null? 0 : ((Position)node1).getNumber()) + "-" + (node2 == null? 0 : ((Position)node2).getNumber());
			Position pos = null;
			Position pposi1 = (Position) parents.get(0);
			Position pposi2 = (Position) parents.get(1);

			Profile PKCF = parentPosition.parentProfile;
			int offset = PKCF.getNumOfPosition();
			Position gapPos = null;
			//ArrayList mchildren = new ArrayList();

			if(posi1 != null){
				List<Node> nodes1 = posi1.getNodes();
				List<Node> pnodes1 = pposi1.getNodes();
				//posi1 = null;
				if (nodes1.size() != pnodes1.size()) {
					System.err.println("Parent node size is not same as child!");
				}
				for (int j = 0; j < nodes1.size(); j++){
						Node node1 = nodes1.get(j);
						Node pnode1 = pnodes1.get(j);

						if(node1.getId() == pnode1.getId()){
							gapPos=adjustGapPosition(node1, pnode1, PKCF, parentPosition);
							node1.getParentProfile().setMark(gapPos, 1);
							int pint = gapPos.getNumber();
							if (pint == offset) {
								System.err.println("Same pint as offset");
							}
							//System.out.println(offset+" Gap offset is "+pint);
						}
						//node1.setNodeType(0);

						pos = (Position) PKCF.addNode(offset, node1);
						//System.out.println("20Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node1.getParentProfile().setMark(posi1, 1);
						//System.out.println("30Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node1.setParentPosition(pos);
						//System.out.println("40Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						//posi1.addNode(node1);

				}
				//System.out.println("50Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());

				posi1.getParentProfile().setMark(posi1, 1);//★2011/1/18入れてみた
				//System.out.println("60Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
			}

			if(posi2 != null){
				List<Node> nodes2 = posi2.getNodes();
				List<Node> pnodes2 = pposi2.getNodes();
				//posi2 = null;
				if (nodes2.size() != pnodes2.size()) {
					System.err.println("Parent node size is not same as child!");
				}
				for (int j = 0; j < nodes2.size(); j++){
						Node node2 = nodes2.get(j);
						Node pnode2 = pnodes2.get(j);

						if(node2.getId() == pnode2.getId()){
							gapPos = adjustGapPosition(node2, pnode2, PKCF, parentPosition);
							node2.getParentProfile().setMark(gapPos, 1);
							node2.getParentProfile().printMarks();
							int pint = gapPos.getNumber();
							if (pint == offset) {
								System.err.println("Same pint as offset");
							}
							//System.out.println(offset+" Gap offset is "+pint);
						}
						//node2.setNodeType(0);
						pos = (Position) PKCF.addNode(offset, node2);
						//System.out.println("2Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node2.getParentProfile().setMark(posi2, 1);
						//System.out.println("3Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node2.setParentPosition(pos);
						//System.out.println("4Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						//posi2.addNode(node2);

				}
				//System.out.println("5Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());

				posi2.getParentProfile().setMark(posi2, 1);//★2011/1/18入れてみた
				//System.out.println("6Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
			}

			//System.out.println(".*.*.* these nodes become position " + pos.getNodes().toString());

			parentPosition.addChildNode(pos); //ギャップの場合は？


			backtrack.add(pos);

			// do recursion
			String currentkey = (posi1 == null? 0 :((Position)posi1).getNumber()) + "-" + (posi2 == null? 0:((Position)posi2).getNumber());
			ArrayList mchildren = (ArrayList) mappings.get(currentkey);

			if(mchildren != null){
				printMappings(mchildren, m, pos);
//				printMappings(mchildren, m, parentPosition);
			}
			m.remove(0);
			m.remove(0);

		}
	}


	/**
	 * 作成中20121213～ printMappingsの部分
	 * determine positions by backtracking mappings
	 * マックスペアよりリーフ側のアラインメントされたノードをPKCFに入れる
	 * childrenを見て同じナンバーだったらそのノードをギャップにする
	 * @param m Mapped Nodes アラインメントポジションぺア(PKCFに入れるノードたち)
	 * @param parents Parent nodes of mapped nodes
	 * @param parentPosition Position in PKCF of Mapped nodes

	 */

	public void pairPosiAlignment(ArrayList<Position> m, ArrayList<Position> parents, Position parentPosition) {

//		System.out.println("Enter printmappings with "+m.toString());
//		System.out.println("Parents are "+parents.toString());
//		System.out.println("parentPosition is " + parentPosition.getNodes().toString());

		while (m.size() > 1) {//アラインメントペアがある場合のみPKCFに入れる作業を行う
			Position posi1 = (Position)m.get(0);
			Position posi2 = (Position)m.get(1);

			if(posi1 == null|| posi2 == null){
				break;
			}

			//String previouskey = (node1 == null? 0 : ((Position)node1).getNumber()) + "-" + (node2 == null? 0 : ((Position)node2).getNumber());
			Position pos = null;
			Position pposi1 = (Position) parents.get(0);
			Position pposi2 = (Position) parents.get(1);

			Profile PKCF = parentPosition.parentProfile;
			int offset = PKCF.getNumOfPosition();
			Position gapPos = null;
			//ArrayList mchildren = new ArrayList();

			if(posi1 != null){
				List<Node> nodes1 = posi1.getNodes();
				List<Node> pnodes1 = pposi1.getNodes();
				//posi1 = null;
				if (nodes1.size() != pnodes1.size()) {
					System.err.println("Parent node size is not same as child!");
				}
				for (int j = 0; j < nodes1.size(); j++){
						Node node1 = nodes1.get(j);
						Node pnode1 = pnodes1.get(j);

						if(node1.getId() == pnode1.getId()){
							gapPos=adjustGapPosition(node1, pnode1, PKCF, parentPosition);
							node1.getParentProfile().setMark(gapPos, 1);
							int pint = gapPos.getNumber();
							if (pint == offset) {
								System.err.println("Same pint as offset");
							}
							//System.out.println(offset+" Gap offset is "+pint);
						}
						//node1.setNodeType(0);

						pos = (Position) PKCF.addNode(offset, node1);
						//System.out.println("20Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node1.getParentProfile().setMark(posi1, 1);
						//System.out.println("30Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node1.setParentPosition(pos);
						//System.out.println("40Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						//posi1.addNode(node1);

				}
				//System.out.println("50Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());

				posi1.getParentProfile().setMark(posi1, 1);//★2011/1/18入れてみた
				//System.out.println("60Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
			}

			if(posi2 != null){
				List<Node> nodes2 = posi2.getNodes();
				List<Node> pnodes2 = pposi2.getNodes();
				//posi2 = null;
				if (nodes2.size() != pnodes2.size()) {
					System.err.println("Parent node size is not same as child!");
				}
				for (int j = 0; j < nodes2.size(); j++){
						Node node2 = nodes2.get(j);
						Node pnode2 = pnodes2.get(j);

						if(node2.getId() == pnode2.getId()){
							gapPos = adjustGapPosition(node2, pnode2, PKCF, parentPosition);
							node2.getParentProfile().setMark(gapPos, 1);
							int pint = gapPos.getNumber();
							if (pint == offset) {
								System.err.println("Same pint as offset");
							}
							//System.out.println(offset+" Gap offset is "+pint);
						}
						//node2.setNodeType(0);
						pos = (Position) PKCF.addNode(offset, node2);
						//System.out.println("2Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node2.getParentProfile().setMark(posi2, 1);
						//System.out.println("3Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						node2.setParentPosition(pos);
						//System.out.println("4Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
						//posi2.addNode(node2);

				}
				//System.out.println("5Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());

				posi2.getParentProfile().setMark(posi2, 1);//★2011/1/18入れてみた
				//System.out.println("6Gap num "+(gapPos == null ? "" : gapPos.getNumber())+" pos number "+pos.getNumber());
			}

			//System.out.println(".*.*.* these nodes become position " + pos.getNodes().toString());

			parentPosition.addChildNode(pos); //ギャップの場合は？


			backtrack.add(pos);

			// do recursion
			String currentkey = (posi1 == null? 0 :((Position)posi1).getNumber()) + "-" + (posi2 == null? 0:((Position)posi2).getNumber());
			ArrayList mchildren = (ArrayList) mappings.get(currentkey);

			if(mchildren != null){
				printMappings(mchildren, m, pos);
//				printMappings(mchildren, m, parentPosition);
			}
			m.remove(0);
			m.remove(0);

		}
	}

	/**
	 * Create Gap Position in new profile to accommodate Gap Nodes.
	 * Cannot add parent position here for new PKCF
	 * Cannot add children positions yet here - created later in printMappings
	 * ギャップノードをPKCFに入れたときギャップを作った糖鎖にマーク１をつけれてない。
	 * @param node Child node of new gap node
	 * @param pnode Parent node of new gap node
	 * @param PKCF Resulting Profile containing new Gap Position
	 * @param parentPosition Position in PKCF of Child node
	 * @returns newly created Gap Position
	 */
	private Position adjustGapPosition(Node node, Node pnode, Profile PKCF, Position parentPosition) {
		//System.out.println(node.getId()+"-"+pnode.getId());
		Node gapnode = makeGap(node, pnode);
		Position nodepos = pnode.getParentPosition();
		int pint = parentPosition.getNumber();
		PKCF.removeNode(pint, node);
		Position pos = (Position) PKCF.addNode(pint, gapnode);
		//parentPosition.addChildNode(pos);
		gapnode.getParentProfile().printMarks();

		nodepos.removeNode(node);
		nodepos.addNode(gapnode);
		return pos;
	}


	private Node makeGap(Node node, Node pnode) {
		Glycan g = node.getParentGlycan();
		int num_of_nodes = g.getNodeListSize(); //g.getNumNodes();
		//pnode = (Node)node.getParent();
		//pnode.children.clear();
		Position nodeparentpos = pnode.parent;//20121203 ギャプノードの親ノードをとってくる
		nodeparentpos.children.remove(node);//20121207nodeparentpos.childrenを消す作業を入れる←ギャップの子供だけ消す//pnode.children.remove(node);//

		Node gap_node = new Node("parent of "+node.getSugar(), g);
		gap_node.setId(num_of_nodes+1);
		gap_node.setBond("", -1, -1);
		gap_node.setNodeType(1); //NodeType 1 = Gap
		gap_node.setLayer(pnode.getLayer());//20121203これ以降にある子ノードのlayerを変える必要があるのでは

		//Position nodeparentpos = node.parent;//20121203 ギャプノードの親ノードをとってくる

		node.setParent(gap_node);
		gap_node.addChildNode(node);
		gap_node.setParent(nodeparentpos);//20121203 pnodeの親をgap_nodeの親として設定する必要がある(前→)gap_node.setParent(pnode)；
		//nodeparentpos.children.remove(nodeparentpos.children.indexOf(node));//20121207nodeparentpos.childrenを消す作業を入れる←ギャップの子供だけ消す
		nodeparentpos.addChildNode(gap_node);//20121203 pnodeの親の子ノードをgap_nodeにする必要がある(前→)pnode.addChildNode(gap_node);

		ArrayList allNodes = (ArrayList) g.getAllNodes();
		if(!allNodes.contains(gap_node)){
			g.nodelist.add(gap_node);
			g.addNode(num_of_nodes, gap_node);
		}

		return gap_node;
	}




	/**
	 * Calculate score of all link pairs.
	 * @param rootlink1
	 * @param rootlink2
	 * @param w
	 * @param maxScore
	 * @param bestMapping
	 * @return
	 */

	public ArrayList getLinkScore(Node rootlink1, Node rootlink2, HashMap w, double maxScore, ArrayList bestMapping) {

		String key = rootlink1.getId() + "-" + rootlink2.getId();
		//System.out.println("rootlink1 is " +rootlink1.toString() + " named " + rootlink1.getSugar());
		//System.out.println("rootlink2 is " +rootlink2.toString() + " named " + rootlink2.getSugar());

		if(scores.containsKey(key)){
			ArrayList returnArray = new ArrayList();
			returnArray.add(maxScore);
			returnArray.add(bestMapping);
			return(returnArray);
		}

		List<Position> children1 = rootlink1.getChildren();
		List<Position> children2 = rootlink2.getChildren();

		if(children1.size() > 0){

			for(int i = 0; i < children1.size(); i++){
				getLinkScore((Node) children1.get(i), rootlink2, w, maxScore, bestMapping);
			}
		}

		if(children2.size() > 0){

			for(int i = 0; i < children2.size(); i++){
				getLinkScore(rootlink1, (Node) children2.get(i), w, maxScore, bestMapping);
			}
		}

		double gapPenaltyScore = -10; /*this score should be optimal. please change the value later*/
		double bestScore = 0; /* compare with sumScore, if sumScore is bigger, bestScore is replaced*/

		List score4Mapping = new ArrayList();
		HashSet returnMapping = new HashSet();
		ArrayList storage = new ArrayList();

		if (children1.size() > 0 || children2.size() > 0) {
			//System.out.println("parents are " + rootlink1 +" and " + rootlink2);
			returnMapping = getMapping(children1, children2, storage,	returnMapping);

			//System.out.println("returnMapping is "+ returnMapping.toString());
			List mapElement = new ArrayList();

			for (Iterator i = returnMapping.iterator(); i.hasNext();) {
				double sumScore = 0;
				List mappingGroup = (List) i.next();

				//mapElement.add(mappingGroup);
				mapElement.clear();

				for (Iterator j = mappingGroup.iterator(); j.hasNext();) {
					Node mappingElement1 = (Node) j.next();
					Node mappingElement2 = (Node) j.next();

					mapElement.add(mappingElement1);
					mapElement.add(mappingElement2);

					if(mappingElement1 != null && mappingElement2 != null){
						key = mappingElement1.getId() + "-"	+ mappingElement2.getId();
						//System.out.println("Trying to get scores for "+mappingElement1.getId() + " and " + mappingElement2.getId());

						sumScore += (Double) scores.get(key);
					}
				}

				if (bestScore <= sumScore) {
					bestScore = sumScore;
					score4Mapping = new ArrayList(mapElement);

					//System.out.println(bestScore+" current bestmapping4: "+score4Mapping.toString());
				}
			}

			/* score4... score of rootlink1 and rootlink2 (ClustalW)*/

			double score4 = 0;
			String name1 = rootlink1.getParentGlycan().getName();
			String name2 = rootlink2.getParentGlycan().getName();

			double weight1 = (Double) w.get(name1);
			double weight2 = (Double) w.get(name2);

			//System.out.println("weights are" + weight1 +" and " + weight2);

			score4 = scoreOfPair(rootlink1, rootlink2) * weight1 * weight2 + bestScore;



			/* score1...score is 0*/

			double score1 = 0;
			List score1Mapping = new ArrayList();



			/* score2...score of rootlink2 and children of rootlink1*/

			double score2 = 0;
			List score2Mapping = new ArrayList();
			double calcOfScore2 = 0;

			for (Iterator k = children1.iterator(); k.hasNext();) {
				Node c1iterator = (Node) k.next();
				key = c1iterator.getId() + "-" + rootlink2.getId();
				calcOfScore2 = (Double) scores.get(key) + gapPenaltyScore;

				if (score2 < calcOfScore2){
					score2 = calcOfScore2;
					score2Mapping = new ArrayList();
					score2Mapping.add(c1iterator);
					score2Mapping.add(rootlink2);
				}
			}



			/* score3... rootlink1 and children of rootlink2*/

			double score3 = 0;
			List score3Mapping = new ArrayList();
			double calcOfScore3 = 0;

			for (Iterator k = children2.iterator(); k.hasNext();) {
				Node c2iterator = (Node) k.next();
				key =  rootlink1.getId() + "-" + c2iterator.getId();
				calcOfScore3 = (Double) scores.get(key) + gapPenaltyScore;

				if (score3 < calcOfScore3){
					score3 = calcOfScore3;
					score3Mapping = new ArrayList();
					score3Mapping.add(rootlink1);
					score3Mapping.add(c2iterator);
				}
			}

			//System.out.println("score3 is "+ score3);


			/*determine maxscore and its mapping*/

			if (maxScore < score1) {
				maxScore = score1;
				bestMapping.clear();
				bestMapping.addAll(score1Mapping);
			}

			if (maxScore < score2) {
				maxScore = score2;
				bestMapping.clear();
				bestMapping.addAll(score2Mapping);
			}

			if (maxScore < score3) {
				maxScore = score3;
				bestMapping.clear();
				bestMapping.addAll(score3Mapping);
			}

			if (maxScore < score4) {
				maxScore = score4;
				bestMapping.clear();
				bestMapping.addAll(score4Mapping);
			}

			key = rootlink1.getId() + "-" + rootlink2.getId();

			scores.put(key, maxScore);
			mappings.put(key, bestMapping.clone());


			//System.out.println("key" + key +": "+ scores.get(key));
			//System.out.println("maxScore: "+ maxScore);
			//System.out.println("bestMapping for "  + key+ " is " + bestMapping);


		}else {/*in case of rootlink1 and rootlink2 are both leaves*/

			String name1 = rootlink1.getParentGlycan().getName();
			String name2 = rootlink2.getParentGlycan().getName();

			double weight1 = (Double) w.get(name1);
			double weight2 = (Double) w.get(name2);

			key = rootlink1.getId() + "-" + rootlink2.getId();
			//System.out.println("key is leave " + key);

			double value = scoreOfPair(rootlink1, rootlink2) * weight1 * weight2;
			scores.put(key, value);
		}

		ArrayList returnArray = new ArrayList();
		returnArray.add(maxScore);
		returnArray.add(key);

		if (maxScore > allMaxScore){
			allMaxPair = key;
			allMaxScore = maxScore;
		}

		return (returnArray);
	}

	/**
	 * Calculate score of all link pairs.
	 * @param Position rootlink1
	 * @param Position rootlink2
	 * @param w
	 * @param maxScore
	 * @param bestMapping
	 * @return
	 */

	public ArrayList getLinkScore(Position rootlink1, Position rootlink2, HashMap w, double maxScore, ArrayList bestMapping) {

		String key = rootlink1.getNumber() + "-" + rootlink2.getNumber();
		//System.out.println(key);//0から始まる
		//String ordernum1 = String.valueOf(rootlink1.getNumber());
		//String ordernum2 = String.valueOf(rootlink2.getNumber());
		System.out.println(rootlink1.toString() + "---" + rootlink2.toString());
		//System.out.println(rootlink1.myNodes.get(ordernum1+1).getSugar() + "---" + rootlink2.myNodes.get(ordernum2+1).getSugar());//できない
		//System.out.println("rootlink1 is " +rootlink1.toString() + " named " + rootlink1.getSugar());
		//System.out.println("rootlink2 is " +rootlink2.toString() + " named " + rootlink2.getSugar());

		if(scores.containsKey(key)){
			ArrayList returnArray = new ArrayList();
			returnArray.add(maxScore);
			returnArray.add(bestMapping);
			return(returnArray);
		}

		List<Position> children1 = rootlink1.getChildren();
		List<Position> children2 = rootlink2.getChildren();

		if(children1.size() > 0){

			for(int i = 0; i < children1.size(); i++){

				getLinkScore(children1.get(i), rootlink2, w, maxScore, bestMapping);
			}
		}

		if(children2.size() > 0){

			for(int i = 0; i < children2.size(); i++){

				getLinkScore(rootlink1, children2.get(i), w, maxScore, bestMapping);
			}
		}

		//★2011.9.15ギャップペナルティを変更できるようにした↓★
		//double gapPenaltyScore = -10; /*this score should be optimal. please change the value later*/

		double bestScore = 0; /* compare with sumScore, if sumScore is bigger, bestScore is replaced*/
		List score4Mapping = new ArrayList();
		HashSet returnMapping = new HashSet();
		ArrayList storage = new ArrayList();

		if (children1.size() > 0 || children2.size() > 0) {//比較ノードが子供ノードを持ってる場合の作業

			//System.out.println("parents are " + rootlink1 +" and " + rootlink2);

			returnMapping = getMapping(children1, children2, storage,	returnMapping);

			//System.out.println("returnMapping is "+ returnMapping.toString());

			List mapElement = new ArrayList();

			for (Iterator i = returnMapping.iterator(); i.hasNext();) {

				double sumScore = 0;
				List mappingGroup = (List) i.next();

				//mapElement.add(mappingGroup);

				mapElement.clear();

				for (Iterator j = mappingGroup.iterator(); j.hasNext();) {

					Position mappingElement1 = (Position) j.next();
					Position mappingElement2 = (Position) j.next();

					mapElement.add(mappingElement1);
					mapElement.add(mappingElement2);

					if(mappingElement1 != null && mappingElement2 != null){

						key = mappingElement1.getNumber() + "-"	+ mappingElement2.getNumber();

						//System.out.println("Trying to get scores for "+mappingElement1.getId() + " and " + mappingElement2.getId());

						sumScore += (Double) scores.get(key);
					}
				}

				if (bestScore <= sumScore) {

					bestScore = sumScore;
					score4Mapping = new ArrayList(mapElement);

					//System.out.println(bestScore+" current bestmapping4: "+score4Mapping.toString());
				}
			}

			double score4 = getScore4(rootlink1, rootlink2, w, bestScore);

			/* score1...score is 0*/
			double score1 = 0;
			List score1Mapping = new ArrayList();


			/* score2...score of rootlink2 and children of rootlink1*/
			double score2 = 0;
			List score2Mapping = new ArrayList();
			double calcOfScore2 = 0;

			for (Iterator k = children1.iterator(); k.hasNext();) {

				Position c1iterator = (Position) k.next();
				key = c1iterator.getNumber() + "-" + rootlink2.getNumber();
				calcOfScore2 = (Double) scores.get(key) + gapPenaltyscore;

				if (score2 < calcOfScore2){

					score2 = calcOfScore2;
					score2Mapping = new ArrayList();
					score2Mapping.add(c1iterator);
					score2Mapping.add(rootlink2);
				}
			}


			/* score3... rootlink1 and children of rootlink2*/
			double score3 = 0;
			List score3Mapping = new ArrayList();
			double calcOfScore3 = 0;

			for (Iterator k = children2.iterator(); k.hasNext();) {

				Position c2iterator = (Position) k.next();
				key =  rootlink1.getNumber() + "-" + c2iterator.getNumber();
				calcOfScore3 = (Double) scores.get(key) + gapPenaltyscore;

				if (score3 < calcOfScore3){

					score3 = calcOfScore3;
					score3Mapping = new ArrayList();
					score3Mapping.add(rootlink1);
					score3Mapping.add(c2iterator);
				}
			}

			//System.out.println("score3 is "+ score3);


			/*determine maxscore and its mapping*/
			if (maxScore < score1) {

				maxScore = score1;
				bestMapping.clear();
				bestMapping.addAll(score1Mapping);
			}

			if (maxScore < score2) {

				maxScore = score2;
				bestMapping.clear();
				bestMapping.addAll(score2Mapping);
			}

			if (maxScore < score3) {

				maxScore = score3;
				bestMapping.clear();
				bestMapping.addAll(score3Mapping);
			}

			if (maxScore < score4) {

				maxScore = score4;
				bestMapping.clear();
				bestMapping.addAll(score4Mapping);
			}

			key = rootlink1.getNumber() + "-" + rootlink2.getNumber();
			scores.put(key, maxScore);
			mappings.put(key, bestMapping.clone());

			System.out.println("key" + key +": "+ scores.get(key));
			//System.out.println("maxScore: "+ maxScore);
			//System.out.println("bestMapping for "  + key+ " is " + bestMapping);


		}else {/*in case of rootlink1 and rootlink2 are both leaves*/
			double value = this.getScore4(rootlink1, rootlink2, w, 0);
/*
			String name1 = rootlink1.getParentGlycan().getName();
			String name2 = rootlink2.getParentGlycan().getName();
			double weight1 = (Double) w.get(name1);
			double weight2 = (Double) w.get(name2);
			key = rootlink1.getId() + "-" + rootlink2.getId();
			//System.out.println("key is leave " + key);
			double value = scoreOfPair(rootlink1, rootlink2) * weight1 * weight2;
*/
			scores.put(key, value);
			System.out.println("key" + key +": "+ scores.get(key));
		}

		ArrayList returnArray = new ArrayList();
		returnArray.add(maxScore);
		returnArray.add(key);

		if (maxScore > allMaxScore){

			allMaxPair = key;
			allMaxScore = maxScore;
		}

		return (returnArray);
	}


	/**
	 * @param rootlink1
	 * @param rootlink2
	 * @param w
	 * @param bestScore
	 * @return
	 */
	private double getScore4(Position rootlink1, Position rootlink2, HashMap w, double bestScore) {
		/* score4... score of rootlink1 and rootlink2 (ClustalW)*/

		double score4 = 0;

		List<Node> nodes1 = rootlink1.getNodes();
		List<Node> nodes2 = rootlink2.getNodes();

		int count=0;

		for (Iterator it1 = nodes1.iterator(); it1.hasNext();) {
			Node n1 = (Node)it1.next();

			for (Iterator it2 = nodes2.iterator(); it2.hasNext();) {
				Node n2 = (Node)it2.next();

				String name1 = n1.getParentGlycan().getName();

				String name2 = n2.getParentGlycan().getName();

				if (name2.length() == 0){
					System.err.println("Glycan with no name!!!!");
				}

				double weight1 = (Double) w.get(name1);

				double weight2 = (Double) w.get(name2);

				//System.out.println("weights are" + weight1 +" and " + weight2);

				score4 += scoreOfPair(n1, n2) * weight1 * weight2 ;

				count++;

			}
		}

		score4 /= count;

		score4 += bestScore;
		return score4;
	}



	/**
	 *
	 * Create all patterns of mapping
	 * @param larger 比較子ノード１
	 * @param smaller 比較子ノード２
	 * @param storage
	 * @param returnMapping
	 * @return
	 *
	 */
	public HashSet getMapping(List<Position> larger, List<Position> smaller, ArrayList storage, HashSet returnMapping) {

		if (smaller.size() == 0 && larger.size() > 0) {//比較子ノード２がない場合[node][null]…をの作業

			ArrayList innermap = new ArrayList();

			for (int i = 0; i < larger.size(); i++) {

				innermap.add(larger.get(i));
				innermap.add(null);
			}

			ArrayList storageclone = (ArrayList) storage.clone();
			storageclone.addAll(innermap);
			returnMapping.add(storageclone);

		}else if (smaller.size() > 0 && larger.size() == 0) {//比較子ノード１がない場合[null][node]…の作業

			ArrayList innermap = new ArrayList();

			for (int i = 0; i < smaller.size(); i++) {

				innermap.add(null);
				innermap.add(smaller.get(i));
			}

			ArrayList storageclone = (ArrayList) storage.clone();
			storageclone.addAll(innermap);
			returnMapping.add(storageclone);

		} else if (smaller.size() > 0 && larger.size() > 0) {

			for (int i = 0; i < larger.size(); i++) {

				for(int j = 0; j < smaller.size(); j++){

					ArrayList innermap = new ArrayList();
					innermap.add(larger.get(i));
					innermap.add(smaller.get(j));

					ArrayList largerclone = new ArrayList(larger);
					largerclone.remove(i);

					ArrayList smallerclone = new ArrayList(smaller);
					smallerclone.remove(j);

					ArrayList storageclone = (ArrayList) storage.clone();
					storageclone.addAll(innermap);

					if (largerclone.size() == 0) {

						returnMapping.add(storageclone);

					} else {

						returnMapping = getMapping(largerclone, smallerclone, storageclone, returnMapping);
					}
				}

				// System.out.println("Returned from recursion with storage "+storage.toString());
			}
		}

		return (returnMapping);
	}

/**

 * Score of links are calculate here.

 * @param pair1
 * @param pair2
 * @return

 */

	public double scoreOfPair(Node pair1, Node pair2) {

		double score = 0;

		String sugar1 = pair1.getSugar();
		String sugar2 = pair2.getSugar();

		String anomer1 = pair1.getAnomer() == null ? " ": pair1.getAnomer().toString();
		String anomer2 = pair2.getAnomer() == null ? " " : pair2.getAnomer().toString();

		int myedge1 = pair1.getMyNum();
		int myedge2 = pair2.getMyNum();

		int parentedge1 = pair1.getParentNum();
		int parentedge2 = pair2.getParentNum();



		if (sugar1.equals(sugar2)) {
			score += sugarscore;
		}

		if (anomer1.equals(anomer2)) {
			score += anomerscore;
		}

		if (myedge1 == myedge2) {
			score += nreCarbonscore;
		}

		if (parentedge1 == parentedge2) {
			score += reCarbonscore;
		}

		//System.out.println("returning score from scoreOfPair is "+ score);

		return score;

	}

	public void printLayer(Position p){//ポジションp以降の全てのLayerを表示する関数
		System.out.println("Position "+((p.getNumber())+1)+": "+" Layer:"+p.getLayer());
		List<Position>pChildren = p.getChildren();
		int pChildrenSize = pChildren.size();
		if (pChildrenSize >0){
			for (int j =0; j <pChildrenSize; j++){
				Position pchildren_elem = (Position) pChildren.get(j);
				printLayer(pchildren_elem);
			}
		}
	}

}