package MCAWbackup;



import java.io.BufferedReader;

import java.io.FileNotFoundException;

import java.io.File;

import java.io.FileInputStream;

import java.io.IOException;

import java.io.InputStream;

import java.io.InputStreamReader;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.Iterator;

import java.util.List;

import java.util.Vector;



/**
 * @author Sakiko
 *
 */

public class Glycan extends Profile{



	Node rootNode;

	List<Node> nodelist;

	public Glycan(String filename) {
		this("",filename);
	}

	public Glycan(String path, String filename) {

		super(filename);

		if (!filename.endsWith(".txt")) {
			filename = filename + ".txt";
		}

		File file = new File(path, filename);

		nodelist = new ArrayList();

		if (file.exists()){
			FileInputStream inputStream;
			try {
				inputStream = new FileInputStream(filename);
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				readFile(bufferedReader);
			} catch (FileNotFoundException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}

	}

	public Glycan(BufferedReader reader){
		super("");
		nodelist = new ArrayList();
		readFile(reader);
	}



	public Node getRootNode() {

		return rootNode;

	}



	/**

	 *Set node as a root and layer=0
	 * @param rootNodename... node name of root (String)
	 * @return

	 */

	public Node setRootNode(String rootNodename) {//rootNodename :ノードの名前

		this.rootNode = new Node(rootNodename,this);

		rootNode.setLayer(0);

		nodelist.add(rootNode);

		super.addNode(0, rootNode);

		return rootNode;

	}



	/**

	 * make a node by specifying a nodename(String), parentNode(Node), anomer(String), myNum(int), parentNum(int)
	 * @param nodename ...name of node
	 * @param parentNode ...to make connection of parent-child.
	 * @param anomer ...alpha or beta
	 * @param myNum ...glycosidic bond information(non-reducing end side)
	 * @param parentNum ...glycosidic bond information(reducing end side)
	 * @return

	 */

	public Node addNode(String nodename, Node parentNode, String anomer, int myNum, int parentNum) {

		if(nodelist.contains(parentNode)){

			//System.out.println("anomer: " +anomer + " myNum:  "+ myNum+" parentNum: " + parentNum);

			Node node = new Node(nodename, this);

			nodelist.add(node);

			node.setParent(parentNode);//子供に親を関連付ける(親子関係をつける)セットレイヤー

			node.setBond(anomer, myNum, parentNum);

			parentNode.addChildNode(node);

			super.addNode(super.getNumNodes(), node);

			return node;

		}

		return null;

	}



	/**

	 * make a node by specifying a nodename(String), parentNode(Node). glycosidic bond information is not required.
	 * @param nodename
	 * @param parentNode
	 * @return

	 */

	public Node addNodeInCaseOfRoot(String nodename, Node parentNode) {

		if(nodelist.contains(parentNode)){

			Node node = new Node(nodename, this);

			nodelist.add(node);

			node.setParent(parentNode);

			parentNode.addChildNode(node);

			super.addNode(super.getNumNodes(), node);

			return node;

		}

		return null;

	}

	public Node addMissingRootNode(String nodename) {


			Node node = new Node(nodename, this);

			nodelist.add(node);

			this.rootNode.setParent(node);

			node.addChildNode(this.rootNode);

			Position newpos = super.addNode(super.getNumNodes(), node);
			super.setMark(newpos, 2);
			super.setRootPosition(newpos);

			node.setId(nodelist.size());
			node.setBond("", -1, -1);
			node.setNodeType(2);
			node.setLayer(this.getRootNode().getLayer()-1);

			return node;



	}


	public boolean readFile(BufferedReader bufferedReader){

		InputStream inputStream;

		try {
//
//		 	inputStream = new FileInputStream(filename);
//			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

			String line = "";

			HashMap <Integer, String> nodeNames = new HashMap();/*key = node number, value = node name*/
			HashMap <String, String> Xcoordinates = new HashMap();/*key = node number, value = x coordinate*/
			HashMap <Integer, Vector >ParentChild = new HashMap();
			HashMap <Integer, Integer>ChildParent = new HashMap();
			HashMap <Integer, String[]> linkInfo = new HashMap(); /*key = childnode#, value = [0]anomer, [1]myNum, [2]parentNum*/

			double maximumX = -1000;

			int rootNum = 0;



			while(!(line = bufferedReader.readLine()).startsWith("///")){

				line = line.replaceAll("  *", " ");

				if(line.contains("ENTRY")){

					this.setName(line.split(" ")[1]);

					//System.out.println(this.getName());

				}

				if(line.contains("NODE")){

					int contents = Integer.parseInt(line.split(" ")[1]);

					for (int  i=0; i < contents; i++){

						line = bufferedReader.readLine();

						line = line.replaceAll("  *", " ");

						String[] NodeArray= line.split(" ");

						nodeNames.put(Integer.parseInt(NodeArray[1]),(String) NodeArray[2]);

						Xcoordinates.put(NodeArray[1], NodeArray[3]);

						if(maximumX < Double.parseDouble(NodeArray[3])){/* a node which have biggest x coordinate become root*/

							maximumX = Double.parseDouble(NodeArray[3]);

							rootNum = Integer.parseInt(NodeArray[1]);

						}



					}

				}else if (line.contains("EDGE")){

					int contents = Integer.parseInt(line.split(" ")[1]);

					for (int  i=0; i < contents; i++){

						line = bufferedReader.readLine();

						line = line.replaceAll("  *", " ");

						String[] EdgeArray= line.split(" ");

						String[] leftelement = EdgeArray[2].split(":");

						String[] rightelement = EdgeArray[3].split(":");



						double xcoordinate1 = Double.parseDouble((String) Xcoordinates.get(rightelement[0]));

						double xcoordinate2 = Double.parseDouble((String) Xcoordinates.get(leftelement[0]));

						String anomer = "";

						if (line.contains("a")){

							anomer = "a";

						}

						if (line.contains("b")){

							anomer = "b";

						}

						if (xcoordinate1 > xcoordinate2){ /*normal node information of KCF. right is parent, and left is child.*/

							Vector valueArray= (Vector)ParentChild.get(Integer.parseInt(rightelement[0]));

							if (valueArray == null){

								valueArray = new Vector();

							}

							valueArray.add(Integer.parseInt(leftelement[0]));

							ParentChild.put(Integer.parseInt(rightelement[0]), valueArray);

							ChildParent.put(Integer.parseInt(leftelement[0]), Integer.parseInt(rightelement[0]));

							String[] linkArray = new String[3];

							linkArray[0] = anomer;

							if (leftelement.length > 1){

								if(leftelement[1].length() > 1){

									linkArray[1] = leftelement[1].substring(leftelement[1].length()-1);/*linkArray[1] is a glycosidic (myNum)*/

								}else if (isNumber(leftelement[1])){

									linkArray[1] = leftelement[1];

								}

							}

							if (rightelement.length > 1){

								if(rightelement[1].length()>1){

									linkArray[2] = rightelement[1].substring(rightelement[1].length()-1);/*linkArray[2] is a glycosidic (myNum)*/

								}else if(isNumber(rightelement[1])){

									linkArray[2] = rightelement[1];

								}

							}

							//System.out.println("linkArray is: " +linkArray[0] + " : " + linkArray[1] + ": " + linkArray[2]);

							linkInfo.put(Integer.parseInt(leftelement[0]), linkArray);

						}else{ /*xcoordinate1 < xcoordinate2*/

							Vector valueArray= (Vector)ParentChild.get(Integer.parseInt(leftelement[0]));

							if (valueArray == null){

								valueArray = new Vector();

							}

							valueArray.add(Integer.parseInt(rightelement[0]));

							ParentChild.put(Integer.parseInt(leftelement[0]), valueArray);

							ChildParent.put(Integer.parseInt(rightelement[0]), Integer.parseInt(leftelement[0]));

							String[] linkArray = new String[3];

							linkArray[0] = anomer;

							if (rightelement.length > 1){

								if(rightelement[1].length()>1){

									linkArray[1] = rightelement[1].substring(rightelement[1].length()-1);

								}else if(isNumber(rightelement[1])){

									linkArray[1] = rightelement[1];

								}

							}

							if (leftelement.length > 1){

								if(leftelement[1].length()>1){

									linkArray[2] = leftelement[1].substring(leftelement[1].length()-1);

								}else if(isNumber(leftelement[1])){

									linkArray[2] = leftelement[1];

								}

							}

							linkInfo.put(Integer.parseInt(rightelement[0]), linkArray);

						}

					}

				}

			}


			Node rootNode = this.setRootNode((String) nodeNames.get(rootNum));

			rootNode.setId(1);

			super.setRootPosition(rootNode.getParentPosition());



			BFS_sort(rootNode, rootNum, ParentChild, ChildParent, linkInfo, nodeNames,1);

			//printTree(rootNode);

		} catch (IOException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

			return false;

		}

		return true;

	}



	private void printTree(Node node) {

		System.out.println("Node "+node.getId()+ " is "+node.getSugar());

		System.out.println("I have "+node.getChildren().size()+" children");

		List<Position> kids = node.getChildren();

		for (Iterator it = kids.iterator(); it.hasNext();) {

			printTree((Node)it.next());

		}

	}

	/**

	 * Create connections of  parent->children, child->parent

	 * @param parentNode
	 * @param rootNum
	 * @param ParentChild
	 * @param ChildParent
	 * @param linkInfo
	 * @param nodeNames
	 * @param id
	 * @return

	 */

	public int BFS_sort(Node parentNode, int rootNum, HashMap ParentChild , HashMap ChildParent, HashMap linkInfo, HashMap nodeNames, int id){

		Vector childNodeArray = (Vector) ParentChild.get(rootNum);

		if (childNodeArray != null){

			for (Iterator i = childNodeArray.iterator(); i.hasNext();){

				int childNode = (Integer) i.next();

				Node node;

				String[] linkInfoArray = (String[]) linkInfo.get(childNode);

				if(ChildParent.containsKey(childNode)){ //

					node = this.addNode((String) nodeNames.get(childNode), parentNode, (String) linkInfoArray[0],

								linkInfoArray[1]==null ? -1 :	Integer.parseInt(linkInfoArray[1]),

										linkInfoArray[2] ==null? -1: Integer.parseInt(linkInfoArray[2]));

				}else{

					node = this.addNodeInCaseOfRoot((String) nodeNames.get(childNode), parentNode);

				}

				id++;

				node.setId(id);

				//System.out.println("Glycan "+this.getName()+ "; node "+id+" is "+node.getSugar()+ "  -  "+node.getParentNum());
				// TODO: set node type according to name (ie: 0=missing, -=gap)
				node.setNodeType(0);

				parentNode.getParentPosition().addChildNode(node.getParentPosition());//position同士(親子)を設定

				id = BFS_sort(node, childNode, ParentChild, ChildParent, linkInfo, nodeNames,id);

			}

		}

		return id;

	}

	public int getNodeListSize() {
		return nodelist.size();
	}

}