package MCAWbackup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

public class Main {

	/************ Main **********************************************
	 * @param args
	 * 		  args[0]:	weights.txt(makedguidetree),
	 * 		  args[1]:	KCFsfile(input KCFs data),
	 * 		  args[2]:	gap penalty score,
	 * 		  args[3]:	monosaccharide score,
	 * 		  args[4]:	anomer score,
	 * 		  args[5]:	non reducing side carbon number score,
	 * 		  args[6]:	reducing side carbon number score,
	 * default score(args2->6): -10, 60, 20, 20, 20; 2012/02/27,
	 *
	 ****************************************************************
	 *
	 *
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (args.length  < 2){
			System.err.println("Please specify at least input guidetree and KCFs filename ");
			for (int i=0; i<args.length; i++) {
				System.err.println("Argument "+i+" is "+args[i]);
			}
			System.exit(-1);
		}
		String mKCFfilename = args[1];//
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(new File(mKCFfilename));
		} catch (FileNotFoundException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
		InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		File distance = new File(args[0]);
		if (distance.exists()) {
			try {
				MCAW mcaw;
				if (args.length == 7) {//adcanced weighting optionsがある場合
					mcaw = new MCAW(Integer.parseInt(args[2]), Integer.parseInt(args[3]), Integer.parseInt(args[4]), Integer.parseInt(args[5]), Integer.parseInt(args[6]));
				} else {
					mcaw = new MCAW();
				}

				Glycan g;
				Profile g1, g2;
				HashMap w = new HashMap();// w: key(GlycanName)=value(weight)
				ArrayList returned = readWeight(distance);
				w = (HashMap) returned.get(0);
				String[] stack=((String)returned.get(1)).split(" ");//stackにweights個別にしたKCFを入れる
				Stack<Profile> order = new Stack<Profile>();
				String plus = "\\+"; /*regular expression of "+" */
				HashMap kcfGlycans = new HashMap();//kcfGlycans: key(糖鎖名)=value(Glycanクラスに格納されたKCFの)

				String line = null;

				while ((line = getNextEntry(bufferedReader)) != null) {//入力されたKCFsを１つずつとってくる、最後(null)かENTRYに至るまで読む
					g = new Glycan (bufferedReader);//■■■
					kcfGlycans.put(g.getName(), g);
				}

				for (int i = 0; i < stack.length; i++){

					if (stack[i].matches(plus)){
						g1 = (Profile) order.pop();
						//System.out.println("PROFILE G1\n "+g1.toSTRING());
						g2 = (Profile) order.pop();
						//System.out.println("PROFILE G2\n "+g2.toSTRING());

						if(g1.getNumNodes() < g2.getNumNodes()){/* replace g1 and g2 that become KCF1 > KCF2 */
							Profile temp = g1;
							g1 = g2;
							g2 = temp;
						}
						Profile z = mcaw.doAlignment(g1, g2, w); /* execute mcaw*/
						System.out.println("★Alignment★\n" + z.toSTRING());
						order.push(z);

					}else{ //if(＋以外)にする110106

						order.push((Profile)kcfGlycans.get(stack[i]));
					}
				}
				Profile lastProfile = (Profile) order.pop();
				System.out.println(lastProfile.toSTRING());

				/******output file**********/
				/*FileOutputStream fos = new FileOutputStream(lastProfile.getName()+".pkcf2");
				OutputStreamWriter osw = new OutputStreamWriter(fos , "MS932");
				BufferedWriter bw = new BufferedWriter(osw);
				bw.write(lastProfile.toSTRING());
				bw.close();
				osw.close();
				fos.close();*/
		    } catch (Exception e) {
		      e.printStackTrace();
		    }
		}else {
			System.err.println(distance.toString());
		}
	}

	private static String getNextEntry(BufferedReader bufferedReader)
			throws IOException {
		bufferedReader.mark(10000);
		String line = null;
		do {
			line = bufferedReader.readLine();
		} while(line != null && !line.startsWith("ENTRY"));
		bufferedReader.reset();
		return line;
	}

	public static ArrayList readWeight(File f){
		ArrayList returnValue = new ArrayList();
		HashMap whash = new HashMap();
		String lastLine = "";
		double maxweight = 0;
		int count = 0;
		String maxkey = ""; /*conserve array number of max weight*/
		InputStream inputStream;

		try {
			inputStream = new FileInputStream(f);
			InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
			BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
			String line = "";
			while ((line = bufferedReader.readLine()) != null) {
				//System.out.println("line is " + line);
				String[] lineSplit = line.split(" ");
				if (discrimination(lineSplit[1]) == true) {/*in case of line have Gnumber and distance information*/
					double distance = Double.parseDouble(lineSplit[1]);
					//distance = 1/distance;
				    if(maxweight < distance){
						maxkey = lineSplit[0];
						maxweight = distance;
					}
					count++;
					whash.put(lineSplit[0], distance);
				} else if (discrimination(lineSplit[1]) == false) {/*in case of line have alignment order */
					lastLine = line;//lastLine: "G～(glycanname)" "G～(glycannaem)" +
				}
			}
			double maxvalue = (Double) whash.get(maxkey);//maxvalue: weightscoreを正規化するため1番高いスコアを取ってくるらしい
			//double maxvalue = 0;
			for(Iterator it = whash.keySet().iterator(); it.hasNext(); ){
				String gnum = (String)it.next();
				double normalized = (Double)whash.get(gnum)/maxvalue;/*normalize a weight*/
				BigDecimal bi = new BigDecimal(String.valueOf(normalized));/*si sha go nyu*/
			    double n = bi.setScale(2,BigDecimal.ROUND_HALF_UP).doubleValue();
				whash.put(gnum, n);//whash: {"key"="value", "G～(glycanname)"="0.～(正規化した下2桁のweight)", ""="",……}
			}
			returnValue.add(whash);
			returnValue.add(lastLine);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return returnValue;//returnValue: [{whash(GlycanName=weight)},{lastLine}]}
	}

	public static boolean discrimination(String str) {
		try {
			Double.parseDouble(str);
			//System.out.println("str = " + str);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			return false;
		}
		return true;
	}

}
