package MCAWbackup;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

public class Position {

	protected int layer; /*layer 0 is root*/
	protected int number; /*number is position of each node*/
	protected List<Position> children;
	protected Position parent;

	//protected List<Node> myNodes;
	protected HashMap<String,Node> myNodes; //StringはGlycanの名前
	protected Profile parentProfile;
	protected int haba;
	protected int xCoordinate;
	protected int yCoordinate;


	protected Position(Profile parentProfile){
		this.parentProfile = parentProfile;
		children = new ArrayList<Position>();
		//myNodes = new ArrayList<Node>();
		myNodes = new HashMap<String,Node>();
		layer = -1000;
	}

	protected Profile getParentProfile(){
		return parentProfile;
	}

	protected List<Position> getChildren(){
		return children;
	}

	/**
	 * add node at a position
	 * @param node
	 */
	protected void addNode(Node node) {
		//this.myNodes.add(node);
		String key = node.getParentGlycan().getName();
//		if (this.myNodes.containsKey(key) && node.getNodeType() != -1) {
//			System.err.println("Adding node "+node.getSugar()+" in profile "+this.parentProfile.getName());
//			System.err.println("In Position.addNode: MyNodes already contains node "+node.getNodeType()+" from glycan "+key+" at position "+this.getNumber());
//		}
		this.myNodes.put(key, node);
		node.setParentPosition(this);
		//System.out.println("confirm+++" + this.getNodes().toString());
	}

	/**
	 * Remove node from this position
	 * @param node node to remove
	 * @return number indicating how many were removed (0 or 1)
	 */
	protected int removeNode(Node node) {
		Object obj = this.myNodes.remove(node.getParentGlycan().getName());
		node.setParentPosition(null);
		if (obj == null) {
			return 0;
		} else {
			return 1;
		}
	}
	protected List<Node> getNodes() {
		return new ArrayList<Node>(myNodes.values());
	}

	public Position getParent() {
		return parent;
	}

	protected void setParent(Position parent) {
		this.parent = parent;
		this.setLayer(parent.getLayer()+1);//親の子供ノードのlayerをセットしたっぽい
	    //this.setXCoordinate(this.getLayer()*(-8));
	}

	/**
	 * younger bond number is add first
	 * @param childNode
	 */
	protected void addChildNode(Position childNode) {
		if (childNode.getNumber() == this.getNumber()) {
			System.err.println("I'm adding myself!!!!");
		}
		childNode.setLayer(this.getLayer()+1);
		if(parentProfile.isRootPosition(childNode)) {
			parentProfile.setRootPosition(this);
		}
		if(childNode.getNodes().size() > 0){
			int currPbond = ((Node) childNode.getNodes().get(0)).getParentNum();
			//System.out.println("current parent bond is " + currPbond);
			int flag = 0;
			for(int i = 0;  i < this.children.size(); i++){
				Position sib = this.children.get(i);
				Node represent = (Node) sib.getNodes().get(0);
				int prevPbond = (Integer) represent.getParentNum() != -1? represent.getParentNum():-1;
				if(prevPbond > currPbond){/*compare bond number with previous bond and current bond*/
					this.children.add(i, childNode);
					flag = 1;
					break;
				}
			}
			if(flag ==0){
				this.children.add(childNode);
			}
		}else{
			this.children.add(childNode);
		}
		childNode.setParent(this);
	}

	protected int getLayer() {
		return layer;
	}

	protected void setLayer(int layer) {
		this.layer = layer;
	}

	public int getHaba() {
		return haba;
	}

	public void setHaba(int haba) {
		this.haba = haba;
	}

	public int getXCoordinate() {
		return xCoordinate;
	}

	public void setXCoordinate(int coordinate) {
		xCoordinate = coordinate;
	}

	public int getYCoordinate() {
		return yCoordinate;
	}

	public void setYCoordinate(int coordinate) {
		yCoordinate = coordinate;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		//System.err.println("Setting number to "+number);
		this.number = number;
	}

	@Override
	public String toString() {

		return this.myNodes.toString();
	}
}