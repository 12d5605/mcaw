package MCAWbackup;

public class Node extends Position {

	private String sugar, anomer;
	private Glycan parentGlycan;
	private int myNum;	/*child hydroxyl group*/
	private int parentNum; /*parent hydroxyl group*/
	private int id;
	private Position parentPosition;
	private int nodetype; /*0 = residue, 1 = gap, 2 = missing(additional node into root/leave)*/

	protected Node(String sugar, Glycan parentGlycan){
		super(parentGlycan);
		this.sugar = sugar;
		this.parentGlycan = parentGlycan;
		this.parentPosition = null;
		this.id = -1;
		this.anomer = null;
		this.myNum = -1;
		this.parentNum = -1;
		this.nodetype = -1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		super.setNumber(id);
		this.id = id;
	}

	/**
	 * obtain Glycan(object)
	 * @return
	 */
	protected Glycan getParentGlycan (){
		return parentGlycan;
	}

	/**
	 * Setting of glycosidic bond
	 * @param anomer alpha/beta
	 * @param myNum bond number of child
	 * @param parentNum bond number of parent
	 */
	protected void setBond(String anomer, int myNum, int parentNum){
		this.anomer = anomer;
		this.parentNum = parentNum;
		this.myNum = myNum;
	}

	public String getAnomer() {
		return anomer;
	}

	public int getParentNum() {
		return parentNum;
	}

	public int getMyNum() {
		return myNum;
	}

	public String getSugar(){
		return sugar;
	}

	public void setParentPosition(Position pos) {
		parentPosition = pos;
		//System.out.println(this.id +" become  child of "+ parentPosition.myNodes.toString());
	}

	/**
	 * obtain position from a node
	 * @param pos
	 */
	public Position getParentPosition(){
		return parentPosition;
	}

	public String toString() {
		return Integer.toString(id);
	}

	public void setNodeType(int i){
		this.nodetype = i; /*i = 0 or 1 or 2*///0 =node, 1 =gap, 2 =missing
	}

	public int getNodeType(){
		return nodetype; /*i = 0 or 1 or 2*///0 =node, 1 =gap, 2 =missing
	}
}
